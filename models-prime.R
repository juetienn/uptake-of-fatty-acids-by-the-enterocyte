# M1': passive diffusion of FAH and membrane protein E as uptake mechanism ----
# E takes S inside the membrane: AX_outer
# p[1] = VEXT/VIN
# p[2] = [FABP]
# p[3] = [X_outer]
# p[4] = VMAX M1
modelM1.2 <- function(t, y, p){
  
  # Read parameters
  V_upper_cytoplasm <- p[1]
  
  V_in_1 <- V_upper_cytoplasm/2
  V_in_2 <- V_upper_cytoplasm/2
  
  V_leaflet <- p[2]
  
  V_membrane <- 2*V_leaflet
  
  V_ER_membrane <- p[3]
  V_enterocyte <- p[4]
  
  pH_e <- p[5]
  pH_i <- p[6]
  pKa_U <- p[7]
  pKa_V <- p[8]
  pKa_WX <- p[9]
  pKa_Y <- p[10]
  pKa_Z <- p[11]
  kon_U_AH <- p[12]
  koff_U_AH <- p[13]
  kon_U_A <- p[14]
  koff_U_A <- p[15]
  kon_memb_AH <- p[16]
  koff_memb_AH <- p[17]
  kon_memb_A <- p[18]
  koff_memb_A <- p[19]
  kon_FABP_AH <- p[20]
  koff_FABP_AH <- p[21]
  kon_FABP_A <- p[22]
  koff_FABP_A <- p[23]
  k_f <- p[24]
  D_AH <- p[25]
  D_A <- p[26]
  D_FABP <- p[27]
  D_FABPFA <- p[28]
  h <- p[29]
  KM <- p[30]
  
  V_e <- p[31] * V_upper_cytoplasm
  
  vmax <- p[34]
  
  kappa_U <- 10^(pH_e-pKa_U) # ext bound
  kappa_V <- 10^(pH_e-pKa_V) # ext mono
  
  kappa_memb <- 10^(pH_e - pKa_Z) # in protein that is in the plasma membrane
  # fatty acid heads are outside membrane (H+ are only in aqueous medium)
  # but tails in protein (microenvironment)
  
  kappa_W <- 10^(pH_e-pKa_WX) # outerleaflet
  kappa_X <- 10^(pH_i-pKa_WX) # innerleaflet
  kappa_Y <- 10^(pH_i-pKa_Y) # in mono
  kappa_FABP <- 10^(pH_i-pKa_Z) # in FABP
  kappa_ER <- 10^(pH_i-pKa_WX) # in ER
  
  vProtMemb <- vmax*y[4]/(KM + y[4])
  
  # deriv
  # notation A corresponds to A_tot
  # A_X_mic, X_ext_mic, A_ext_mono, A_X_outer, X_outer, A_X_inner, X_inner, A_in1_mono, FABP_1, A_in1_FABP
  # A_in2_mono, FABP_2, A_in2_FABP, A_X_in_ER, X_in_ER
  r <- rep(0,15)
  
  # A_X_mic
  r[1] <- -(koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1] + (kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2]
  # X_ext_mic
  r[2] <- -(kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2] + (koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1]
  # A_ext_mono
  r[3] <- -(kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2] + (koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1] - (V_leaflet/V_e)*(kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] + (V_leaflet/V_e)*(koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4]
  # A_X_outer
  r[4] <- -(koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4] + (kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] - 1/2*k_f*(y[7]*1/(1+kappa_W)*y[4] - y[5]*1/(1+kappa_X)*y[6]) - 2*vProtMemb
  # X_outer
  r[5] <- -(kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] + (koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4] - 1/2*k_f*(y[5]*1/(1+kappa_X)*y[6] - y[7]*1/(1+kappa_W)*y[4]) + 2*vProtMemb
  # A_X_inner
  r[6] <- -(koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] + (kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] - 1/2*k_f*(y[5]*1/(1+kappa_X)*y[6] - y[7]*1/(1+kappa_W)*y[4])
  # X_inner
  r[7] <- -(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] + (koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] - 1/2*k_f*(y[7]*1/(1+kappa_W)*y[4] - y[5]*1/(1+kappa_X)*y[6])
  # A_in1_mono
  r[8] <- -(V_leaflet/V_in_1)*(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] + (V_leaflet/V_in_1)*(koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] - (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (1/h^2)*(D_AH + kappa_Y*D_A)*1/(1+kappa_Y)*(y[11] - y[8]) + (V_membrane/V_in_1)*vProtMemb
  # FABP_1
  r[9] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (D_FABP/h^2)*(y[12] - y[9])
  # A_in1_FABP
  r[10] <- -(koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (D_FABPFA/h^2)*(y[13] - y[10])
  # A_in2_mono
  r[11] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] - (V_ER_membrane/V_in_2)*(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] + (V_ER_membrane/V_in_2)*(koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + (1/h^2)*(D_AH + kappa_Y*D_A)*1/(1+kappa_Y)*(y[8] - y[11])
  # FABP_2
  r[12] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] + (D_FABP/h^2)*(y[9] - y[12])
  # A_in2_FABP
  r[13] <- -(koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] + (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (D_FABPFA/h^2)*(y[10] - y[13])
  # A_X_in_ER
  r[14] <- -(koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + (kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15]
  # X_in_ER
  r[15] <- -(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] + (koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14]
  
  return(list(r))
  
}

# M2': passive diffusion of FAH and intracellular enzyme (ACSL-like) as uptake mechanism ----
# E takes S inside the membrane: AX_in_ER
# p[1] = VEXT/VIN
# p[2] = [FABP]
# p[3] = [X_outer]
# p[4] = VMAX M2
modelM2.2 <- function(t, y, p){
  
  # Read parameters
  V_upper_cytoplasm <- p[1]
  
  V_in_1 <- V_upper_cytoplasm/2
  V_in_2 <- V_upper_cytoplasm/2
  
  V_leaflet <- p[2]
  
  V_membrane <- 2*V_leaflet
  
  V_ER_membrane <- p[3]
  V_enterocyte <- p[4]
  
  pH_e <- p[5]
  pH_i <- p[6]
  pKa_U <- p[7]
  pKa_V <- p[8]
  pKa_WX <- p[9]
  pKa_Y <- p[10]
  pKa_Z <- p[11]
  kon_U_AH <- p[12]
  koff_U_AH <- p[13]
  kon_U_A <- p[14]
  koff_U_A <- p[15]
  kon_memb_AH <- p[16]
  koff_memb_AH <- p[17]
  kon_memb_A <- p[18]
  koff_memb_A <- p[19]
  kon_FABP_AH <- p[20]
  koff_FABP_AH <- p[21]
  kon_FABP_A <- p[22]
  koff_FABP_A <- p[23]
  k_f <- p[24]
  D_AH <- p[25]
  D_A <- p[26]
  D_FABP <- p[27]
  D_FABPFA <- p[28]
  h <- p[29]
  KM <- p[30]
  
  V_e <- p[31] * V_upper_cytoplasm
  
  vmax <- p[34]
  
  kappa_U <- 10^(pH_e-pKa_U) # ext bound
  kappa_V <- 10^(pH_e-pKa_V) # ext mono
  kappa_W <- 10^(pH_e-pKa_WX) # outerleaflet
  kappa_X <- 10^(pH_i-pKa_WX) # innerleaflet
  kappa_Y <- 10^(pH_i-pKa_Y) # in mono
  kappa_FABP <- 10^(pH_i-pKa_Z) # in FABP
  kappa_ER <- 10^(pH_i-pKa_WX) # in ER

  vEnzyme <- vmax*y[14]/(KM + y[14])
  
  # deriv
  # notation A corresponds to A_tot
  # A_X_mic, X_ext_mic, A_ext_mono, A_X_outer, X_outer, A_X_inner, X_inner, A_in1_mono, FABP_1, A_in1_FABP
  # A_in2_mono, FABP_2, A_in2_FABP, A_X_in_ER, X_in_ER, A-CoA
  r <- rep(0,16)
  
  # A_X_mic
  r[1] <- -(koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1] + (kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2]
  # X_ext_mic
  r[2] <- -(kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2] + (koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1]
  # A_ext_mono
  r[3] <- -(kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2] + (koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1] - (V_leaflet/V_e)*(kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] + (V_leaflet/V_e)*(koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4]
  # A_X_outer
  r[4] <- -(koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4] + (kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] - 1/2*k_f*(y[7]*1/(1+kappa_W)*y[4] - y[5]*1/(1+kappa_X)*y[6])
  # X_outer
  r[5] <- -(kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] + (koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4] - 1/2*k_f*(y[5]*1/(1+kappa_X)*y[6] - y[7]*1/(1+kappa_W)*y[4])
  # A_X_inner
  r[6] <- -(koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] + (kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] - 1/2*k_f*(y[5]*1/(1+kappa_X)*y[6] - y[7]*1/(1+kappa_W)*y[4])
  # X_inner
  r[7] <- -(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] + (koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] - 1/2*k_f*(y[7]*1/(1+kappa_W)*y[4] - y[5]*1/(1+kappa_X)*y[6])
  # A_in1_mono
  r[8] <- -(V_leaflet/V_in_1)*(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] + (V_leaflet/V_in_1)*(koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] - (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (1/h^2)*(D_AH + kappa_Y*D_A)*1/(1+kappa_Y)*(y[11] - y[8])
  # FABP_1
  r[9] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (D_FABP/h^2)*(y[12] - y[9])
  # A_in1_FABP
  r[10] <- -(koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (D_FABPFA/h^2)*(y[13] - y[10])
  # A_in2_mono
  r[11] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] - (V_ER_membrane/V_in_2)*(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] + (V_ER_membrane/V_in_2)*(koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + (1/h^2)*(D_AH + kappa_Y*D_A)*1/(1+kappa_Y)*(y[8] - y[11]) 
  # FABP_2
  r[12] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] + (D_FABP/h^2)*(y[9] - y[12])
  # A_in2_FABP
  r[13] <- -(koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] + (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (D_FABPFA/h^2)*(y[10] - y[13])
  # A_X_in_ER
  r[14] <- -(koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + (kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] - vEnzyme 
  # X_in_ER
  r[15] <- -(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] + (koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + vEnzyme
  # A-CoA
  r[16] <- vEnzyme
  
  return(list(r))
  
}

# M3': passive diffusion of FAH, membrane protein/endocytosis and intracellular enzyme (ACSL-like) as uptake mechanism ----
# p[1] = VEXT/VIN
# p[2] = [FABP]
# p[3] = [X_outer]
# p[4] = VMAX M1
# p[5] = VMAX M2
modelM3.2 <- function(t, y, p){
  
  # Read parameters
  V_upper_cytoplasm <- p[1]
  
  V_in_1 <- V_upper_cytoplasm/2
  V_in_2 <- V_upper_cytoplasm/2
  
  V_leaflet <- p[2]
  
  V_membrane <- 2*V_leaflet
  
  V_ER_membrane <- p[3]
  V_enterocyte <- p[4]
  
  pH_e <- p[5]
  pH_i <- p[6]
  pKa_U <- p[7]
  pKa_V <- p[8]
  pKa_WX <- p[9]
  pKa_Y <- p[10]
  pKa_Z <- p[11]
  kon_U_AH <- p[12]
  koff_U_AH <- p[13]
  kon_U_A <- p[14]
  koff_U_A <- p[15]
  kon_memb_AH <- p[16]
  koff_memb_AH <- p[17]
  kon_memb_A <- p[18]
  koff_memb_A <- p[19]
  kon_FABP_AH <- p[20]
  koff_FABP_AH <- p[21]
  kon_FABP_A <- p[22]
  koff_FABP_A <- p[23]
  k_f <- p[24]
  D_AH <- p[25]
  D_A <- p[26]
  D_FABP <- p[27]
  D_FABPFA <- p[28]
  h <- p[29]
  KM_E1 <- p[30]
  KM_E2 <- p[31]
  
  V_e <- p[32] * V_upper_cytoplasm
  
  vmax_E1 <- p[35]
  vmax_E2 <- p[36]
  
  kappa_U <- 10^(pH_e-pKa_U) # ext bound
  kappa_V <- 10^(pH_e-pKa_V) # ext mono
  
  kappa_memb <- 10^(pH_e - pKa_Z) # in protein that is in the plasma membrane
  # fatty acid heads are outside membrane (H+ are only in aqueous medium)
  # but tails in protein (microenvironment)
  
  kappa_W <- 10^(pH_e-pKa_WX) # outerleaflet
  kappa_X <- 10^(pH_i-pKa_WX) # innerleaflet
  kappa_Y <- 10^(pH_i-pKa_Y) # in mono
  kappa_FABP <- 10^(pH_i-pKa_Z) # in FABP
  kappa_ER <- 10^(pH_i-pKa_WX) # in ER
  
  vProtMemb <- vmax_E1*y[4]/(KM_E1 + y[4])
  vEnzyme <- vmax_E2*y[14]/(KM_E2 + y[14])
  # print(vmax_E2)

  # deriv
  # notation A corresponds to A_tot
  # A_X_mic, X_ext_mic, A_ext_mono, A_X_outer, X_outer, A_X_inner, X_inner, A_in1_mono, FABP_1, A_in1_FABP
  # A_in2_mono, FABP_2, A_in2_FABP, A_X_in_ER, X_in_ER, A_CoA
  r <- rep(0,16)
  
  # A_X_mic
  r[1] <- -(koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1] + (kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2]
  # X_ext_mic
  r[2] <- -(kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2] + (koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1]
  # A_ext_mono
  r[3] <- -(kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2] + (koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1] - (V_leaflet/V_e)*(kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] + (V_leaflet/V_e)*(koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4]
  # A_X_outer
  r[4] <- -(koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4] + (kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] - 1/2*k_f*(y[7]*1/(1+kappa_W)*y[4] - y[5]*1/(1+kappa_X)*y[6]) - 2*vProtMemb
  # X_outer
  r[5] <- -(kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] + (koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4] - 1/2*k_f*(y[5]*1/(1+kappa_X)*y[6] - y[7]*1/(1+kappa_W)*y[4]) + 2*vProtMemb
  # A_X_inner
  r[6] <- -(koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] + (kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] - 1/2*k_f*(y[5]*1/(1+kappa_X)*y[6] - y[7]*1/(1+kappa_W)*y[4])
  # X_inner
  r[7] <- -(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] + (koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] - 1/2*k_f*(y[7]*1/(1+kappa_W)*y[4] - y[5]*1/(1+kappa_X)*y[6])
  # A_in1_mono
  r[8] <- -(V_leaflet/V_in_1)*(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] + (V_leaflet/V_in_1)*(koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] - (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (1/h^2)*(D_AH + kappa_Y*D_A)*1/(1+kappa_Y)*(y[11] - y[8]) + (V_membrane/V_in_1)*vProtMemb
  # FABP_1
  r[9] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (D_FABP/h^2)*(y[12] - y[9])
  # A_in1_FABP
  r[10] <- -(koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (D_FABPFA/h^2)*(y[13] - y[10])
  # A_in2_mono
  r[11] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] - (V_ER_membrane/V_in_2)*(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] + (V_ER_membrane/V_in_2)*(koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + (1/h^2)*(D_AH + kappa_Y*D_A)*1/(1+kappa_Y)*(y[8] - y[11])
  # FABP_2
  r[12] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] + (D_FABP/h^2)*(y[9] - y[12])
  # A_in2_FABP
  r[13] <- -(koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] + (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (D_FABPFA/h^2)*(y[10] - y[13])
  # A_X_in_ER
  r[14] <- -(koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + (kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] - vEnzyme 
  # X_in_ER
  r[15] <- -(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] + (koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + vEnzyme
  # A-CoA
  r[16] <- vEnzyme
  
  return(list(r))
  
}

# M4': M3' with no FABP ----
# p[1] = VEXT/VIN
# p[2] = [X_outer]
# p[3] = VMAX M1
# p[4] = VMAX M2
modelM4.2 <- function(t, y, p){
  
  # Read parameters
  V_upper_cytoplasm <- p[1]
  
  V_in_1 <- V_upper_cytoplasm/2
  V_in_2 <- V_upper_cytoplasm/2
  
  V_leaflet <- p[2]
  
  V_membrane <- 2*V_leaflet
  
  V_ER_membrane <- p[3]
  V_enterocyte <- p[4]
  
  pH_e <- p[5]
  pH_i <- p[6]
  pKa_U <- p[7]
  pKa_V <- p[8]
  pKa_WX <- p[9]
  pKa_Y <- p[10]
  pKa_Z <- p[11]
  kon_U_AH <- p[12]
  koff_U_AH <- p[13]
  kon_U_A <- p[14]
  koff_U_A <- p[15]
  kon_memb_AH <- p[16]
  koff_memb_AH <- p[17]
  kon_memb_A <- p[18]
  koff_memb_A <- p[19]
  kon_FABP_AH <- 0 # p[20]
  koff_FABP_AH <- 0 # p[21]
  kon_FABP_A <- 0 # p[22]
  koff_FABP_A <- 0 # p[23]
  k_f <- p[24]
  D_AH <- p[25]
  D_A <- p[26]
  D_FABP <- p[27]
  D_FABPFA <- p[28]
  h <- p[29]
  KM_E1 <- p[30]
  KM_E2 <- p[31]
  
  V_e <- p[32] * V_upper_cytoplasm
  
  vmax_E1 <- p[34]
  vmax_E2 <- p[35]
  
  kappa_U <- 10^(pH_e-pKa_U) # ext bound
  kappa_V <- 10^(pH_e-pKa_V) # ext mono
  
  kappa_memb <- 10^(pH_e - pKa_Z) # in protein that is in the plasma membrane
  # fatty acid heads are outside membrane (H+ are only in aqueous medium)
  # but tails in protein (microenvironment)
  
  kappa_W <- 10^(pH_e-pKa_WX) # outerleaflet
  kappa_X <- 10^(pH_i-pKa_WX) # innerleaflet
  kappa_Y <- 10^(pH_i-pKa_Y) # in mono
  kappa_FABP <- 10^(pH_i-pKa_Z) # in FABP
  kappa_ER <- 10^(pH_i-pKa_WX) # in ER
  
  vProtMemb <- vmax_E1*y[4]/(KM_E1 + y[4])
  vEnzyme <- vmax_E2*y[14]/(KM_E2 + y[14])
  # print(vmax_E2)
  
  # deriv
  # notation A corresponds to A_tot
  # A_X_mic, X_ext_mic, A_ext_mono, A_X_outer, X_outer, A_X_inner, X_inner, A_in1_mono, FABP_1, A_in1_FABP
  # A_in2_mono, FABP_2, A_in2_FABP, A_X_in_ER, X_in_ER, A_CoA
  r <- rep(0,16)
  
  # A_X_mic
  r[1] <- -(koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1] + (kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2]
  # X_ext_mic
  r[2] <- -(kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2] + (koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1]
  # A_ext_mono
  r[3] <- -(kon_U_AH + kappa_V*kon_U_A)*1/(1+kappa_V)*y[3]*y[2] + (koff_U_AH + kappa_U*koff_U_A)*1/(1+kappa_U)*y[1] - (V_leaflet/V_e)*(kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] + (V_leaflet/V_e)*(koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4]
  # A_X_outer
  r[4] <- -(koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4] + (kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] - 1/2*k_f*(y[7]*1/(1+kappa_W)*y[4] - y[5]*1/(1+kappa_X)*y[6]) - 2*vProtMemb
  # X_outer
  r[5] <- -(kon_memb_AH + kappa_V*kon_memb_A)*1/(1+kappa_V)*y[3]*y[5] + (koff_memb_AH + kappa_W*koff_memb_A)*1/(1+kappa_W)*y[4] - 1/2*k_f*(y[5]*1/(1+kappa_X)*y[6] - y[7]*1/(1+kappa_W)*y[4]) + 2*vProtMemb
  # A_X_inner
  r[6] <- -(koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] + (kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] - 1/2*k_f*(y[5]*1/(1+kappa_X)*y[6] - y[7]*1/(1+kappa_W)*y[4])
  # X_inner
  r[7] <- -(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] + (koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] - 1/2*k_f*(y[7]*1/(1+kappa_W)*y[4] - y[5]*1/(1+kappa_X)*y[6])
  # A_in1_mono
  r[8] <- -(V_leaflet/V_in_1)*(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[8]*y[7] + (V_leaflet/V_in_1)*(koff_memb_AH + kappa_X*koff_memb_A)*1/(1+kappa_X)*y[6] - (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (1/h^2)*(D_AH + kappa_Y*D_A)*1/(1+kappa_Y)*(y[11] - y[8]) + (V_membrane/V_in_1)*vProtMemb
  # FABP_1
  r[9] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (D_FABP/h^2)*(y[12] - y[9])
  # A_in1_FABP
  r[10] <- -(koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[10] + (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[8]*y[9] + (D_FABPFA/h^2)*(y[13] - y[10])
  # A_in2_mono
  r[11] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] - (V_ER_membrane/V_in_2)*(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] + (V_ER_membrane/V_in_2)*(koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + (1/h^2)*(D_AH + kappa_Y*D_A)*1/(1+kappa_Y)*(y[8] - y[11])
  # FABP_2
  r[12] <- -(kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] + (D_FABP/h^2)*(y[9] - y[12])
  # A_in2_FABP
  r[13] <- -(koff_FABP_AH + kappa_FABP*koff_FABP_A)*1/(1+kappa_FABP)*y[13] + (kon_FABP_AH + kappa_Y*kon_FABP_A)*1/(1+kappa_Y)*y[11]*y[12] + (D_FABPFA/h^2)*(y[10] - y[13])
  # A_X_in_ER
  r[14] <- -(koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + (kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] - vEnzyme 
  # X_in_ER
  r[15] <- -(kon_memb_AH + kappa_Y*kon_memb_A)*1/(1+kappa_Y)*y[11]*y[15] + (koff_memb_AH + kappa_ER*koff_memb_A)*1/(1+kappa_ER)*y[14] + vEnzyme
  # A-CoA
  r[16] <- vEnzyme

  return(list(r))
  
}
