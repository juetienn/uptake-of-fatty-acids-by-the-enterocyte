################################################################################

Provides a better understanding of the experimental data

Last updated: January 31, 2023

################################################################################

Dataset: from 1 to 7, to enable us to cite the references from which raw (initial) data were extracted
	1: 	Chow, S.L., and Hollander, D. (1979b).
		Linoleic acid absorption in the unaesthetized rat: Mechanism of transport and influence of luminal factors on absorption.
		Lipids 14, 378–385.
		https://doi.org/10.1007/BF02533421.
	2: 	Stremmel, W. (1988).
		Uptake of fatty acids by jejunal mucosal cells is mediated by a fatty acid binding membrane protein.
		J. Clin. Invest. 82, 2001–2010.
		https://doi.org/10.1172/JCI113820. 
	3:	Chow, S.L., and Hollander, D. (1979a).
		A  dual, concentration-dependent absorption mechanism of linoleic acid by rat jejunum in vitro.
		J. Lipid Res. 20, 349–356. 
		https://doi.org/10.1016/S0022-2275(20)40617-0
	4:	Goré, J., and Hoinard, C. (1993).
		Linolenic Acid Transport in Hamster Intestinal Cells Is Carrier-Mediated.
		J. Nutr. 123, 66–73.
		https://doi.org/10.1093/jn/123.1.66.
	5:	Tranchant, T., Besson, P., Hoinard, C., Delarue, J., Antoine, J.M., Couet, C., and Gore, J. (1997).
		Mechanisms and kinetics of alpha-linolenic acid uptake in Caco-2 clone TC7. Biochim. Biophys.
		Acta BBA-Lipids Lipid Metab. 1345, 151–161.
		https://doi.org/10.1016/S0005-2760(96)00171-3. 
	6:	Murota, K., and Storch, J. (2005).
		Uptake of Micellar Long-Chain Fatty Acid and sn-2-Monoacylglycerol into Human Intestinal Caco-2 Cells Exhibits 	
		Characteristics of Protein-Mediated Transport.
		J. Nutr. 135, 1626–1630.
		https://doi.org/10.1093/jn/135.7.1626. 
	7:	Goré, J., Hoinard, C., and Couet, C. (1994).
		Linoleic acid uptake by isolated enterocytes: Influence of α-linolenic acid on absorption.
		Lipids 29, 701–706.
		https://doi.org/10.1007/BF02538914.
	8 & 9: 
		Punchard et al. (2000) Analysis of the intestinal absorption of essential fatty acids in vivo in the rat. 
		https://doi.org/10.1054/plef.1999.0121 
		
Author: first author of the paper

Year: year of publication of the paper

FIG: figure from which data were extracted

UptakeIni: in the units given by the paper (raw data)

FaextInit: in the units given by the paper (raw data)

UptakeConverted: nmol / mg protein / min 
Conversion according to UnitConversion.pdf (table of conversion)

FaextConverted: µM (micromolar = micromoles/liter)
If FAini are monomers, used K to convert units
In the case of Murota, used values resulted from interpolation
Murota-2005_interpolation_OA_2-MG.csv (interpolation.R)

CollectionTime: time at which samples were collected to compute initial uptake points (in seconds)

FAType: name of the fatty acid considered in the figure

Species: species on which the experiments were made

Team: to separate the different teams of authors 
	A: S.L. Chow & D. Hollander (USA)
	B: W. Stremmel (Germany)
	D: J. Goré & C. Hoinard (France)
	E: K. Murota & J. Storch (USA)
	F: N.A. Punchard, A.T. Green, J.G.L. Mullins, R.P.H Thompson (UK)

curveID: ID is unique per set of points (curves) (not needed for the paper)

LowerIni: Lower error bar related to the point (initial units)

UpperIni: Upper error bar related to the point (initial units)

Lower: Lower error bar related to the point (converted units)

Upper: Upper error bar related to the point (converted units)

SdIni: Standard deviation (initial units) as given in the paper (Stremmel, Murota, Punchard) or computed from the SE (standard error to the mean) (for other papers)

SemIni: Standard error of the mean (initial units) given in the paper (others) or computed from the SD (Stremmel, Murota, Punchard) 

SdConverted: Standard deviation (converted units) as given in the paper (Stremmel, Murota, Punchard) or computed from the SE (standard error to the mean) (for other papers)

SemConverted: Standard error of the mean (converted units) given in the paper (others) or computed from the SD (Stremmel, Murota, Punchard)

n: Number of replicates (used to compute S.E.M or S.D.) 

pH: pH value of the experimental solution used for uptake measurements as given in the considered paper 

