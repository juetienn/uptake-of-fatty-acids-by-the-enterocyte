# Libraries
library(FME)
library(deSolve)
library(MASS)

# Source ---- 
source("usefulFunctions.R")
source("models-prime.R")
args <- "uniform"
source("parameters.R")
# LOAD EXPERIMENTAL DATA
source("datasets.R")

# TRAKCING ----
# DATAFRAME to keep track of positivity problems
DF.POSITIVITYPROBLEMS <- data.frame(0,0,0,0,0,0,0,0,0,0,
                                    0,0,0,0,0,0,0,0,0,0,
                                    0,0,0,0,88)
colnames(DF.POSITIVITYPROBLEMS) <- c("Time", "AXmic", "Xmic", "Amonoout", "AXout",
                                     "Xout", "AXin", "Xin", "Amonoin1", "FABP1",
                                     "AinFABP1", "Amonoin2", "FABP2", "AinFABP2",
                                     "AXer", "Xer", "ACoA",
                                     "vextvin", "concFABP", "concXouter", "vMax1", "vMax2",
                                     "FA0", "modelnum", "dataset")

# Initialize ---- 

MODEL_TO_RUN <- 3.2

INI_CONDITIONS <- "centered"
initialParameterValues <- setInitialValues(MODEL_TO_RUN, INI_CONDITIONS)
iniValuesList <- list("par" = initialParameterValues)

whenToSaveInterFiles <- NITERATIONS_LONG+100

# print useful data 
printUsefulData(MODEL_TO_RUN, FALSE,
                description = "Uniform priors, DRSCALE=0.5 and updatecov=100")

# MCMC ----

print("Stremmel")
stremmel.mcmc <- fittingDataset.modMCMC.explicit(stremmel, iniValuesList, MODEL_TO_RUN,
                                                 WhenToSave = whenToSaveInterFiles,
                                                 NameOfTheTmpFiles="_stremmel-ad3p")
saveRDS(stremmel.mcmc, file=paste0(FILEPATH, runDate,"_stremmel-ad3-prime.rds"))

DF.POSITIVITYPROBLEMS <- rbind(DF.POSITIVITYPROBLEMS, c(rep(0,24),5))

print("Murota")
murota.mcmc <- fittingDataset.modMCMC.explicit(murota, iniValuesList, MODEL_TO_RUN,
                                               WhenToSave = whenToSaveInterFiles,
                                               NameOfTheTmpFiles="_murota-ad3p")
saveRDS(murota.mcmc, file=paste0(FILEPATH, runDate,"_murota-ad3-prime.rds"))

DF.POSITIVITYPROBLEMS <- rbind(DF.POSITIVITYPROBLEMS, c(rep(0,24),93))

print("Gore93")
gore93.mcmc <- fittingDataset.modMCMC.explicit(gore93, iniValuesList, MODEL_TO_RUN,
                                               WhenToSave = whenToSaveInterFiles,
                                               NameOfTheTmpFiles="_gore93-ad3p")
saveRDS(gore93.mcmc, file=paste0(FILEPATH, runDate,"_gore93-ad3-prime.rds"))

DF.POSITIVITYPROBLEMS <- rbind(DF.POSITIVITYPROBLEMS, c(rep(0,24),94))

print("Gore94")
gore94.mcmc <- fittingDataset.modMCMC.explicit(gore94, iniValuesList, MODEL_TO_RUN,
                                               WhenToSave = whenToSaveInterFiles,
                                               NameOfTheTmpFiles="_gore94-ad3p")
saveRDS(gore94.mcmc, file=paste0(FILEPATH, runDate,"_gore94-ad3-prime.rds"))

DF.POSITIVITYPROBLEMS <- rbind(DF.POSITIVITYPROBLEMS, c(rep(0,24),792))

print("Chow79")
chow79.mcmc <- fittingDataset.modMCMC.explicit(chow79, iniValuesList, MODEL_TO_RUN,
                                               WhenToSave = whenToSaveInterFiles,
                                               NameOfTheTmpFiles="_chw-ad3p")
saveRDS(chow79.mcmc, file=paste0(FILEPATH, runDate,"_chow79-ad3-prime.rds"))

DF.POSITIVITYPROBLEMS <- rbind(DF.POSITIVITYPROBLEMS, c(rep(0,24),791))

print("Chow79dual")
chow79dual.mcmc <- fittingDataset.modMCMC.explicit(chow79dual, iniValuesList, MODEL_TO_RUN,
                                                   WhenToSave = whenToSaveInterFiles,
                                                   NameOfTheTmpFiles="_chwd-ad3p")
saveRDS(chow79dual.mcmc, file=paste0(FILEPATH, runDate,"_chow79dual-ad3-prime.rds"))

DF.POSITIVITYPROBLEMS <- rbind(DF.POSITIVITYPROBLEMS, c(rep(0,24),97))

print("Tranchant97")
tranchant97.mcmc <- fittingDataset.modMCMC.explicit(tranchant97, iniValuesList, MODEL_TO_RUN,
                                                    WhenToSave = whenToSaveInterFiles,
                                                    NameOfTheTmpFiles="_trc-ad3p")
saveRDS(tranchant97.mcmc, file=paste0(FILEPATH, runDate,"_tranchant97-ad3-prime.rds"))

DF.POSITIVITYPROBLEMS <- rbind(DF.POSITIVITYPROBLEMS, c(rep(0,24),182))

print("Punchard 2000 18:2")
punchard182.mcmc <- fittingDataset.modMCMC.explicit(punchard182, iniValuesList, MODEL_TO_RUN,
                                                    WhenToSave = whenToSaveInterFiles,
                                                    NameOfTheTmpFiles="_p182-ad3p")
saveRDS(punchard182.mcmc, file=paste0(FILEPATH, runDate,"_punchard182-ad3-prime.rds"))

DF.POSITIVITYPROBLEMS <- rbind(DF.POSITIVITYPROBLEMS, c(rep(0,24),183))

print("Punchard 2000 18:3")
punchard183.mcmc <- fittingDataset.modMCMC.explicit(punchard183, iniValuesList, MODEL_TO_RUN,
                                                    WhenToSave = whenToSaveInterFiles,
                                                    NameOfTheTmpFiles="_p183-ad3p")
saveRDS(punchard183.mcmc, file=paste0(FILEPATH, runDate,"_punchard183-ad3-prime.rds"))

writeInFilePositivityProblems(paste0(FILEPATH, runDate,"_M3p-track-pos-errors.csv"),
                              DF.POSITIVITYPROBLEMS)
