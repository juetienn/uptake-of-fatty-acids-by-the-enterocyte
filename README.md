Thank you for your interest in our data and topic.
Here is a guide to help you through our files.

Last updated: January 28, 2023

# PROJECT

A digital mirror for the enterocyte:
A quantitative model of long-chain fatty acid intestinal uptake

# DESCRIPTION OF THE PROJECT

We developed a quantitative and mechanistic model of the intestinal uptake of 
long-chain fatty acids. To understand the critical and important molecular steps 
of long-chain fatty acid uptake by the enterocyte, we studied this full model 
with and without specific protein mechanisms. Simulating a gene knockout is 
equivalent to removing modules: 
The E-KO model: the model without the active transport mechanism, 
The FABP-KO model: the model without the FAPB module, 
The ACSL-KO model: the model without ACSL (intracellular metabolism).

Note that in the code, models are called as followed:
- Full model (all modules): M3-prime (M3p, M3', 3.2)
- ACSL-KO model: M1-prime (M1p, M1', 1.2)
- FABP-KO model: M4-prime (M4p, M4', 4.2)
- E-KO model: M2-prime (M2p, M2', 2.2)

# REQUIRED

R should be installed as well as the R interface, R Studio.
The R libraries also required are 
- knitr
- DeSolve
- FME
- coda
- MetBrewer
- plotrix
- MASS

# HOW TO USE

1. More details on the data may be find at https://doi.org/10.15454/8MQUUB (Data INRAe portal)
2. The analysis of models may be run by running models-analysis.Rmd
3. The file mcmc-one-dataset.Rmd gives an example of MCMC analysis without the need for simulations to be run. The required MCMC objects are provided as .rds files in the "Objects" folder.
4. If one wants to go further in the MCMC analysis, files are provided for analysis of MCMC chains on all experimental datasets.

**To use the mcmc-all-datasets-one-chain.Rmd file**, one needs to run simulations first to create the MCMC objects as .rds files. 
To do so: 
- Run the MCMC simulations on all models (default date - runDate - appearing in simulation files is the system date)
  - Either run scripts-parallel-run_ad_paper.sh (to run all MCMC simulation files at once)
  - Or run each runMCMC-M*.R files in turn using Rscript runMCMC-M*.R (* being the model 
  id. as given in the files description below)
- Run the script to obtain the .rds files required to plot envelope around model fits 
  (i) In the file, indicate the date (as loadDate) appearing in the MCMC .rds files 
  (ii) Rscript runSensRange.R 
- Run the script to obtain an interval of fluxes for the full model
  (i) In the file, indicate the date (as loadDate) appearing in the MCMC .rds files 
  (ii) Rscript runFluxes.R 
- Analyze the output by filling the required dates as indicated in the section "SET SIMULATION DATES" of the .Rmd file.

**To use the mcmc-all-datasets-two-chains.Rmd file:**
- Run simulations twice to create two MCMC objects (.rds files) by 
model with two different dates.
- In the file, fill the required dates as indicated in the section "SET SIMULATION DATES" of the .Rmd file.

# DESCRIPTION OF THE FILES

Codes are in the main directory ".".
Simulation files are saved and loaded from the "./Objects" directory.

################################################################################

**models.R**

contains the models implemented when the substrate is aqueous FA monomers.

################################################################################

**models-prime.R** 

contains the models implemented when the substrate is FA monomers 
inserted into a membrane leaflet.
Models used for the article.

################################################################################

**parameters.R**

determines all parameter values.

################################################################################

**usefulFunctions.R**

contains all the functions used in the scripts (except some 
very specific plot functions).

################################################################################

**2022-10-31_UptakeData_errorbars.csv** is the "database" file, it contains all 
experimental uptake data digitized from published articles.

2022-10-31_UptakeData_errorbars.ods > 2022-10-31_UptakeData_errorbars.csv

Information and details may be found in the 2022-10-31_UptakeData_errorbars.ods file at https://doi.org/10.15454/8MQUUB (Data INRAe portal)

################################################################################

**interpolation.R**

enabled us to convert monomers of fatty acids into total fatty acids.
Created the file Murota-2005_interpolation_OA_2-MG.csv

################################################################################

**Murota-2005_interpolation_OA_2-MG.csv**


This file results from the code interpolation.R
We added a row name "Row" to enable good compatibility with the Data INRAE portal.

################################################################################

**UnitConversion_avril2021.pdf**

Explains the method we set up to convert uptake data into the same units.
The result is the file UptakeData_7datasets_paper.ods
which we use in the global code as UptakeData_7datasets_paper.csv

The code is such that, for the csv file, we have :
decimal as ","
separator as "\t"

################################################################################

**datasets.R** 

loads all datasets from the "database" file.

################################################################################

**models-analysis.Rmd**

studies all models using parameter values taken from the literature.

################################################################################

**mcmc-one-dataset.Rmd**

is an example of MCMC analysis on one experimental dataset. 
MCMC files are provided so that there is no need to run (heavy) simulations.

################################################################################

**mcmc-all-datasets-one-chain.Rmd**

allows for analysis of MCMC simulations output on all datasets. MCMC files are not provided.

################################################################################

**mcmc-all-datasets-two-chains.Rmd**

allows for analysis of MCMC simulations output on all datasets when two MCMC 
chains have been run. MCMC files are not provided.

################################################################################

**runMCMC-M...**

are the files used to run MCMC simulations. 
Parameters can be specified in the files (initial values, priors type...).
They may take several days depending on the number of iterations.

MCMC objects are saved as .rds files that can be further loaded by code scripts 
for analysis.

The date appearing in the .rds file names is the system date at which the files started to run.

################################################################################

**runSensRange.R** 

enables to run sensRange on all datasets from previously run MCMC 
objects (.rds). Use to plot envelopes around the best fit as returned by the 
MCMC algorithm for all prime models. 

Requires the date (as loadDate) of the MCMC chain saved as .rds.

################################################################################

**runFluxes.R** 

enables to run fluxes on dose-responses for 1,000 different parameters sets of a one-chain MCMC output in order to plot 95% credible intervals on fluxes values. It is currently coded for the full model. 

Requires the date (as loadDate) of the MCMC chain saved as .rds.

################################################################################



