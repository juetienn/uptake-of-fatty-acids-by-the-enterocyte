#!/bin/bash

echo "Starting running R scripts in the background"

nohup Rscript --verbose runMCMC-M3p.R &> output-m3p_ad.out &
nohup Rscript --verbose runMCMC-M1p.R &> output-m1p_ad.out &
nohup Rscript --verbose runMCMC-Mp.R &> output-m2p_ad.out &
nohup Rscript --verbose runMCMC-M4p.R &> output-m4p_ad.out &
nohup Rscript --verbose runMCMC-M3.R &> output-m3_ad.out &
